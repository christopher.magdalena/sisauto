CREATE DATABASE  IF NOT EXISTS `sisauto` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sisauto`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sisauto
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `nome_fantasia` varchar(45) DEFAULT NULL,
  `razao_social` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `id_tpServico` int(11) NOT NULL,
  PRIMARY KEY (`id_empresa`),
  KEY `fk_empresa_tpServico_idx` (`id_tpServico`),
  CONSTRAINT `fk_empresa_tpServico` FOREIGN KEY (`id_tpServico`) REFERENCES `tpservico` (`id_tpServico`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Destak Rodas','Suspensão','-16.0562','-47.9830',1),(2,'Ceu e Mar','Oficina','-16.0566','-47.9830',2),(3,'Karlao Auto Eletrica','Auto Elétrica','-16.0754','-47.9878',3),(4,'Borracharia Padre Cicero','Borracharia','-16.0731','-47.9858',4),(5,'Borracharia Bino','Borracharia','-16.0735','-47.9859',4),(6,'Belo Auto Eletrica','Auto Elétrica','-16.0805','-47.9756',3),(7,'Pneu DF','Oficina','-16.0537','-47.9826',2),(8,'Pneu DF','Suspensão','-16.0537','-47.9826',1),(9,'MultCustom Suspensões','Suspensão','-16.0744','-47.9734',1);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tpservico`
--

DROP TABLE IF EXISTS `tpservico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpservico` (
  `id_tpServico` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `descricao` text,
  PRIMARY KEY (`id_tpServico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tpservico`
--

LOCK TABLES `tpservico` WRITE;
/*!40000 ALTER TABLE `tpservico` DISABLE KEYS */;
INSERT INTO `tpservico` VALUES (1,'Suspensao','Suspensao'),(2,'Mecânica','Serviços em Motor'),(3,'Elétrica','Serviços de eletrica'),(4,'Borracharia','Pneus para Carro e Moto');
/*!40000 ALTER TABLE `tpservico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'chris','dudacris_2@yahoo.com.br','$2y$10$N2xE0YyfjCEgGn5iqMVS.e5H6Lg2OUrZ0rOfiCTVydIG37UvTMqCe','S02rVKBQHCtnIEaub1qy7eaqWxct2sHbBY3Df6GKuWeEm1EXg2OjzBuqGwSE','2018-12-11 16:42:48','2018-12-11 16:42:48'),(2,'marcelo','marcelo@email.com','$2y$10$5q8OEshxOlxL1EypUe67suApdV6x1TxIpJ6IO6BUvooEMLAb.o3eG','85EUGdRZC7vjvbOrQqLaEdsnKzquHwKI8C4YgqAFvdoPOS3kLHx0IhkRpTlL','2018-12-12 13:20:57','2018-12-12 13:20:57'),(3,'Anderson Pires','anderson@email.com','$2y$10$668qObr2vnz.jWlt5A0PrORB3rG4mp.wUod6Zb/jIEgmQp.N6chHu','PE9lUGzkVwugXIx949KO3giWcumIa7aiolrp2RcNiGqZ0hWKzTpFAwcUmXev','2018-12-12 18:45:13','2018-12-12 18:45:13'),(4,'Noelly','Noelly@email.com','$2y$10$HGqPl23lz7ITdz9aZ0rlh.Fiq/qd1NibWv79uke0nA2DBrG2BDKnm','5ZSpIEmyBEjcibfiSNGpbqulsXNArwjWtrnBrfp2FioHVGoJx9UHdJwzn0yE','2018-12-12 18:53:45','2018-12-12 18:53:45'),(5,'Gleuton Dutra','gleuton@email.com','$2y$10$X4PUCY9VWoM7eRT4Doi7Ger1zWIdmYh9BPObVqk0jHBmDNeh/NhIu','uqgEBqqYZLK0SgJzUEgRB1TP0RUv09VPswDaieCRw8rypxTEND28dzUGi6k0','2018-12-12 19:57:19','2018-12-12 19:57:19'),(7,'Ednelia','ednelia@email.com','$2y$10$gfZ2.lY8VMl3/2e6ExNpuOieMOTLyot4Sk55jvK8BwmW2s7x2FDxW','1lWBgyiQpd9lXxQCnptNED8OpiVns3qQQ330SAlOJH29usqJTUsSD0IyVokf','2018-12-13 15:22:46','2018-12-13 15:22:46'),(8,'levy alves da silva','lasss7@gmail.com','$2y$10$teVutrFcjBR9kE.pmYgLe.vPvzL783tZPRvLIpOL1P6okryShHppe','BYIAYh6raeZdQkWjakz4gAmGv2nKtRZjIAgBqWO7RBAcDSsC4TyDwjicvCJY','2018-12-13 19:32:13','2018-12-13 19:32:13'),(9,'520320651','520320651@email.com','$2y$10$lnV9sFbpHyl0GDSLfVJJpOzk31Da1pgYiva/sX9LsRo.pNwW9PDE6','YyHnpVRkKJs3vAam1ri3H0Qi2DqT4O61ANRFJpjYxmJwq4ux736wHIrscBFQ','2018-12-13 20:15:55','2018-12-13 20:15:55'),(10,'William','william@email.com','$2y$10$qGzadExpx.IY1.JxUgyZD..YrekH447C7N7ItOHuWm3G8gnDdj8qW','KUMjVhcnbNQmPS4mYmgTcwctrfftSxHh68SnPx67F9d3En3Kjzf2a6Nbqrav','2018-12-14 22:56:23','2018-12-14 22:56:23'),(11,'Cicero','cicero@email.com','$2y$10$EoDJEnHYtWIZiegfoLzrhO02zNtMWXqSyEN1spz2mEep/t0J04MK6','mxhmV0IlrtqO28iV8ddz1JslgimyH6a14bU39T6iRhYn7paqnAOYkOwci5Rx','2018-12-14 23:44:26','2018-12-14 23:44:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-03 16:41:07
