var lat = parseFloat($('#lat').val().replace(',','.'));
var lng = parseFloat($('#lng').val().replace(',','.'));

function initMap() {
    //map options
    var teste = {lat: lat, lng: lng};
    var googleM = google.maps;
    var map = new googleM.Map(document.getElementById('map'), {
        zoom: 13,
        center: teste
    });
    var usuario = new googleM.Marker({
        position: teste,
        map: map,
        title: 'Unidade'
    });
    navigator.geolocation.getCurrentPosition(locationSucess);

    function locationSucess(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var usuario = new googleM.Marker({
            position: {lat: latitude, lng: longitude},
            map: map,
            draggable: true,
            title: 'Você'
        });
    }
}

