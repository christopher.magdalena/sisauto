<?php
/**
 * Author: lucas.vieira
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $connection = "mysql";
    protected $fillable = [
            "nome",
            'descricao',
        ];
    public $timestamps = false;
    protected $table = "tpservico";
    protected $primaryKey = "id_tpServico";
}