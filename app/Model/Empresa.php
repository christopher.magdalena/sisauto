<?php
/**
 * Author: lucas.vieira
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $connection = "mysql";
    protected $fillable
        = [
            'nome_fantasia',
            'razao_social',
            'latitude',
            'longitude',
            'id_tpServico'
        ];
    public $timestamps = false;
    protected $table = "empresa";
    protected $primaryKey = "id_empresa";


    public function servicos()
    {
        return $this->belongsTo('\App\Model\Servico', 'id_tpServico');
    }
}