<?php
/**
 * Author: lucas.vieira
 */

namespace App\Repositories;

use App\Model\Empresa;

class EmpresaRepository extends Repository
{
    protected $model;

    public function __construct(Empresa $lojista)
    {
     $this->model = $lojista;
    }

    public function getByTpServico(int $coTpServico)
    {
       return $this->model->where('id_tpServico',$coTpServico)->get();
    }

}