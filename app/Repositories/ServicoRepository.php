<?php
/**
 * Author: lucas.vieira
 */

namespace App\Repositories;

use App\Model\Servico;


class ServicoRepository extends Repository
{
    protected $model;

    public function __construct(Servico $servico)
    {
        $this->model = $servico;
    }
}