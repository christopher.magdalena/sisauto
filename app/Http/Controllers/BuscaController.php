<?php
/**
 * Created by PhpStorm.
 * User: christopher.silva
 * Date: 28/11/2018
 * Time: 15:46
 */

namespace App\Http\Controllers;


use App\Repositories\EmpresaRepository;
use App\Repositories\ServicoRepository;

class BuscaController extends Controller
{
    private $servicoRepository;
    private $empresaRepository;
    public function __construct(ServicoRepository $servicoRepository, EmpresaRepository $empresaRepository)
    {
        $this->servicoRepository = $servicoRepository;
        $this->empresaRepository = $empresaRepository;
        $this->middleware('auth');
    }
    public function buscaServico()
    {
        $tiposServicos = $this->servicoRepository->all();

        return view('busca.busca')->with(compact('tiposServicos'));
    }

    public function buscaEmpresa(int $id_tpServico)
    {

        $empresas = $this->empresaRepository->getByTpServico($id_tpServico);

        return view('empresa.list')->with(compact('empresas'));
    }
}
