<?php
/**
 * Created by PhpStorm.
 * User: christopher.silva
 * Date: 28/11/2018
 * Time: 15:46
 */

namespace App\Http\Controllers;

class MapaController extends Controller
{
    public function view(string $latitude, string $longitude)
    {
        return view('mapa.busca')->with(compact('latitude'))->with(compact('longitude'));

    }
}
