<style>
    td {text-align: left;}

</style>

<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">Estabelecimentos:</th>
        <th scope="col">Serviço</th>
        <th scope="col">Mapa</th>
        <th scope="col">Avaliar</th>
    </tr>
    </thead>
    <tbody>
    @foreach($empresas as $empresa)
        <tr>
            <td scope="row">{{$empresa->nome_fantasia}}</td>
            <td>{{$empresa->servicos->nome}}</td>
            <td>
                <a class="glyphicon glyphicon-map-marker" style="color: #ffffff" aria-hidden="true"
                   href="/mapa/{{str_replace('.',',',$empresa->latitude)}}/{{str_replace('.',',',$empresa->longitude)}}"}}></a>
            </td>
            <td>
                <a class="glyphicon glyphicon-star-empty" style="color: #ffffff" aria-hidden="true"></a>
            </td>
        </tr>
     @endforeach
    </tbody>
</table>