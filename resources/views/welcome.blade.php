@extends('template.app')
@section('title','Home')
@section('main')
    @parent
    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
                 data-settings='{"stellar-background-ratio": 0.6}'
                 data-toggle="parallax-bg">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-12">
                        <a class="hero-brand" href="/" title="Home"><img alt="SisAme Logo"
                                                                         src="{{ asset('img/logo_grande.png')}}"></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <h1>
                        SisAuto
                    </h1>

                    <p class="tagline">
                        Localize as oficinas mecânicas mais próximas de você!
                    </p>
                    <a class="btn btn-full btn-about" href="/como-funciona">Saiba Mais!</a>
                </div>
            </div>

        </section>

        <section class="about">
            <div class="container text-center">
                <h1>
                    Sobre SisAuto
                </h1>

                <h2>
                    O SisAuto, Sistema para busca de Serviços Automotivos,
                    é um WebMobile desenvolvido por alunos da Faculdade JK para facilitar
                    a localização de oficinas mecânicas.
                </h2>

            </div>
        </section>
        <!-- /About -->
        <!-- convite para Cadastrar -->

        <section class="cta">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-sm-12 text-lg-left text-center">
                        <h2>
                            Cadastre-se
                        </h2>

                        <p>
                            Cadastre-se no SisAuto e utilize nossos serviços!
                        </p>
                    </div>

                    <div class="col-lg-3 col-sm-12 text-lg-right text-center">
                        <a class="btn btn-ghost" href="/cadastro-cliente">Cadastro Cliente</a>
                    </div><br>
                    <div class="col-lg-3 col-sm-12 text-lg-right text-center">
                        <a class="btn btn-ghost" href="/cadastro-empresa">Cadastro Lojista</a>
                    </div>
                </div>
            </div>
        </section>
@endsection