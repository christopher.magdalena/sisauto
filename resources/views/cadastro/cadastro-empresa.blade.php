@extends('template.app')
@section('title', 'Cadastro de Empresa')
@section('main')
    @parent
    <style>
        label {
            color: white};
    </style>
    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
             data-settings='{"stellar-background-ratio": 0.6}'
             data-toggle="parallax-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Credenciamento de Empresa</h1>
                    <form action="/save-empresa" method="post">
                        <input type="hidden" name="_token" value=" {{ csrf_token() }}">
                        <div class="form-group">
                            <label>Nome Fantasia</label>
                            <input type="text" class="form-control" name="nome-fantasia" placeholder="Nome Fantasia">
                        </div>
                        <div class="form-group">
                            <label>Razão Social</label>
                            <input type="text" class="form-control" name="razao-social" placeholder="Razão Social">
                        </div>
                        <div class="form-group">
                            <label>CNPJ</label>
                            <input type="number" class="form-control" name="cnpj" placeholder="CNPJ">
                        </div>

                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <label>Telefone</label>
                            <input type="number" class="form-control" name="telefone" placeholder="Telefone">
                        </div>
                        <div class="form-group">
                            <label>CEP</label>
                            <input type="number" class="form-control" name="cep" placeholder="CEP">
                        </div>
                        <div class="form-group">
                            <label for="FormControlSelect1">Tipos de Serviços:</label><br><br>
                            {{--<select class="form-control" name="tipo-servico" id="FormControlSelect1">--}}
                            {{--<option>Blindagem</option>--}}
                            {{--<option>Mecânica em Geral</option>--}}
                            {{--<option>Retifica</option>--}}
                            {{--<option>Lanternagem/Pintura</option>--}}
                            {{--<option>Elétrica</option>--}}
                            {{--<option>Suspensão</option>--}}
                            {{--</select>--}}
                            <input type="checkbox" name="service1" value="Blindagem">Blindagem<br>
                            <input type="checkbox" name="service2" value="Mecanica">Mecânica em Geral<br>
                            <input type="checkbox" name="service3" value="Retifica">Retifica<br>
                            <input type="checkbox" name="service3" value="Pintura">Lanternagem/Pintura<br>
                            <input type="checkbox" name="service3" value="Elétrica">Elétrica<br>
                            <input type="checkbox" name="service3" value="Suspensão">Suspensão<br>
                        </div>
                        <button type="submit" class="btn btn-success">Cadastrar</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection