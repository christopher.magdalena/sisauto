@extends('template.inside')
@section('title','Busca')
@section('main')

    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
             data-settings='{"stellar-background-ratio": 0.6}'
             data-toggle="parallax-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Busca de Serviços</h1>
                    <form class="form-inline">
                        <div class="form-group mb-2">
                            <label style="color: white">Tipos de Serviços:</label>
                            <div>
                                <select name="especialidade" id="especialidade" class="form-control especialidade">
                                    @foreach($tiposServicos as $tipoServico)
                                        <option value="{{$tipoServico->id_tpServico}}">{{$tipoServico->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary mb-2" id="pesquisar">Pesquisar</button>
                    </form>
                    <br/>
                    <div id='result'></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('#pesquisar').click(function(){
            var coServico=$("#especialidade").val();
            $.get( "lista-de-empresas/" + coServico, function( data ) {
                $( "#result" ).html( data );
            });
            return false;
        })
    </script>
@endsection