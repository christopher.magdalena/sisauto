@extends('template.inside')
@section('title','Mapa')
@section('main')

    <input type="hidden" id="lat" value="{{$latitude}}">
    <input type="hidden" id="lng" value="{{$longitude}}">


    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
             data-settings='{"stellar-background-ratio": 0.6}'
             data-toggle="parallax-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Localização do Serviço:</h1>
                    <br/>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-10 col-xs-12">
                            <div id="map"></div>
                            <button class="btn btn-primary mb-2"><a href="/busca">Voltar</a></button>
                        </div>
                     </div>
                    </div>
                    </div>
                </div>
    </section>


@endsection

@section('script')
    <script src="{{asset('/js/maps.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyCKfC-5lLpDU0Wy6hHAqq8UbMmL4typA&callback=initMap">
    </script>
@endsection