<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{'/node_modules/bootstrap3/dist/css/bootstrap.css'}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('/css/font-awesome.css')}}" type="text/css">
    <link href="{{'/css/style.css'}}" rel="stylesheet">
</head>

<header>
    <div class="container">

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div id="logo" class="pull-left">
                <a href="/"><img src="{{ asset('/img/logo_branca.png')}}" alt="" title=""/></a>
            </div>
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbarCollapse"
                            aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">SisAuto</a>
                </div>
                <div id="navbarCollapse" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/busca">Busca</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Sair
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<main class="col-sm-12">
    @section('main')
    @show
</main>
<script src="{{'/node_modules/jquery/dist/jquery.js'}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{'/node_modules/bootstrap3/dist/js/bootstrap.js'}}"></script>
@section('script')
@show
</html>