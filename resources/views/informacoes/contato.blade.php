@extends('template.app')
@section('title','Contato')
@section('main')
    @parent
    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
             data-settings='{"stellar-background-ratio": 0.6}'
             data-toggle="parallax-bg">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12">
                    <h1>Fale Conosco</h1>
                    </br>
                    <form class="col-md-12 col-lg-12 col-sm-12 col-xs-12" action="/" method="POST">
                        <input type="hidden">
                        <div class="input-group-1">
                            <label style="color: white;">Nome:</label>
                            <input type="text" class="form-control" name="nome" placeholder="Nome">
                            <p style="color: red;"></p>
                            </br>
                        </div>
                        <div class="input-group-1">
                            <label style="color: white;">Telefone:</label>
                            <input type="tel" class="form-control" name="telefone" placeholder="Telefone">
                            <p style="color: red;"></p>
                            </br>
                        </div>
                        <div class="input-group-1">
                            <label style="color: white;">E-mail:</label>
                            <input type="email" class="form-control" name="email" placeholder="E-mail">
                            <p style="color: red;"></p>
                            </br>
                        </div>
                        <div class="input-group-1">
                            <label style="color: white;">Assunto:</label>
                                <textarea name="mensagem" rows="5" class="form-control"
                                          placeholder="Escreva aqui sua Mensagem"></textarea>
                            <p style="color: red;"></p>
                            </br>
                        </div>
                        <div class="input-group-1">
                            <button class="btn btn-success">ENVIAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection