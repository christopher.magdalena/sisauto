@extends('template.app')
@section('title','Como Funciona')
@section('main')
    @parent
    <section class="hero" data-bg-img="{{ asset('img/parallax-bg.jpg')}}"
             data-settings='{"stellar-background-ratio": 0.6}'
             data-toggle="parallax-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Como Funciona</h1>
                    <h3 class="about">
                        O SisAuto, é um sistema para busca de Serviços Automotivos,
                        é um WebMobile desenvolvida por alunos da Faculdade JK para facilitar
                        a localização de oficinas mecânicas.
                    </h3>
                    <h3 class="about">
                        O Sistema disponibilizará ao usuário um rol de estabelecimentos
                        credenciados que ofereça o serviço desejado, a distância, e as avaliações
                        para que você possa escolher o serviço mais conveniente.
                    </h3>
                </div>
            </div>
        </div>
    </section>

@endsection