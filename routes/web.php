<?php

use \Illuminate\Support\Facades\{Auth, Route};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@home');
Route::get('/como-funciona', 'HomeController@comoFunciona');
Route::get('/contato', 'HomeController@contato');
Route::get('/cadastro-cliente', 'CadastrarController@cadastroCliente')->name('cadCliente');
Route::get('/cadastro-empresa', 'CadastrarController@cadastroEmpresa')->name('cadEmpresa');
Route::post('/save-empresa', 'CadastrarController@saveEmpresa');
Route::post('/save-cliente', 'CadastrarController@saveCliente');
Route::get('/busca', 'BuscaController@buscaServico');
Route::get('/login', 'LoginController@login');
Route::get('/mapa/{latitude}/{longitude}', 'MapaController@view');
Route::get('/lista-de-empresas/{coservico}', 'BuscaController@buscaEmpresa');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
